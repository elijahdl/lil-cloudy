use chrono::{prelude::*, Duration};
use log::debug;
use maplit::hashmap;
use rust_decimal::Decimal;
use serde::{Deserialize, Serialize};
use serde_with::{serde_as, DisplayFromStr};

use crate::{util::get_weather_icon, WeatherReport};

pub(crate) trait CloudyWeatherData {
  fn create_cloudy_weather_data(self, now: &DateTime<Utc>) -> WeatherReport;
}

#[derive(Debug, Deserialize, Serialize)]
pub(crate) struct WttrCache {
  pub weather: WeatherReport,
  pub last_fetch_time: DateTime<Utc>,
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub(crate) struct WttrModel {
  pub current_condition: Vec<CurrentCondition>,
  pub nearest_area: Vec<Area>,
  request: Vec<LocationRequest>,
  weather: Vec<Daily>,
}

impl CloudyWeatherData for WttrModel {
  fn create_cloudy_weather_data(self, now: &DateTime<Utc>) -> WeatherReport {
    use CloudyWeatherDataPoint::*;
    let current_condition = self.current_condition[0].clone();
    let area = self.nearest_area[0].clone();
    let today_forecast = self.weather[0].clone();
    let tomorrow_forecast = self.weather[1].clone();

    let (next_sunrise, next_sunset, next_moonrise, next_moonset) = calculate_astronomy(
      &today_forecast.astronomy[0],
      &tomorrow_forecast.astronomy[0],
      now,
    );

    hashmap! {
      "current_feels_like_f".to_string() => Int(current_condition.feels_like_f),
      "current_feels_like_c".to_string() => Int(current_condition.feels_like_c),
      "current_temp_f".to_string() => Int(current_condition.temp_f),
      "current_temp_c".to_string() => Int(current_condition.temp_c),
      "current_cloud_cover".to_string() => Int(current_condition.cloudcover),
      "current_humidity".to_string() => Int(current_condition.humidity),
      "current_local_obs_datetime".to_string() => Str(current_condition.local_obs_datetime),
      "current_precip_inches".to_string() => Dec(current_condition.precip_inches),
      "current_precip_mm".to_string() => Dec(current_condition.precip_mm),
      "current_pressure_hpa".to_string() => Str(current_condition.pressure),
      "current_pressure_inches".to_string() => Str(current_condition.pressure_inches),
      "current_uv_index".to_string() => Int(current_condition.uv_index),
      "current_visibility_miles".to_string() => Int(current_condition.visibility_miles),
      "current_visibility_km".to_string() => Int(current_condition.visibility),
      "current_weather_code".to_string() => Int(current_condition.weather_code),
      "current_weather_description".to_string() => Str(current_condition.weather_desc[0].value.clone()),
      "current_weather_icon".to_string() => Str(get_weather_icon(current_condition.weather_code)),
      "current_wind_direction_16_point".to_string() => Str(current_condition.wind_dir_16_point),
      "current_wind_direction_degree".to_string() => Int(current_condition.wind_dir_degree),
      "current_wind_speed_mph".to_string() => Int(current_condition.wind_speed_miles),
      "current_wind_speed_kmph".to_string() => Int(current_condition.wind_speed_kmph),

      "area_name".to_string() => Str(area.area_name[0].value.clone()),
      "area_country".to_string() => Str(area.country[0].value.clone()),
      "area_latitude".to_string() => Dec(area.latitude),
      "area_longitude".to_string() => Dec(area.longitude),
      "area_population".to_string() => Int(area.population),
      "area_region".to_string() => Str(area.region[0].value.clone()),

      "today_temp_high_f".to_string() => Int(today_forecast.max_temp_f),
      "today_temp_high_c".to_string() => Int(today_forecast.max_temp_c),
      "today_temp_low_f".to_string() => Int(today_forecast.min_temp_f),
      "today_temp_low_c".to_string() => Int(today_forecast.min_temp_c),
      "today_uv_index".to_string() => Int(today_forecast.uv_index),

      "tomorrow_temp_high_f".to_string() => Int(tomorrow_forecast.max_temp_f),
      "tomorrow_temp_high_c".to_string() => Int(tomorrow_forecast.max_temp_c),
      "tomorrow_temp_low_f".to_string() => Int(tomorrow_forecast.min_temp_f),
      "tomorrow_temp_low_c".to_string() => Int(tomorrow_forecast.min_temp_c),
      "tomorrow_uv_index".to_string() => Int(tomorrow_forecast.uv_index),

      "next_sunrise".to_string() => Time(next_sunrise),
      "next_sunset".to_string() => Time(next_sunset),
      "next_moonrise".to_string() => Time(next_moonrise),
      "next_moonset".to_string() => Time(next_moonset),
    }
  }
}

// (Next Sunrise, Next Sunset, Next Moonrise, Next Moonset)
fn calculate_astronomy(
  today: &WttrAstronomy,
  tomorrow: &WttrAstronomy,
  now: &DateTime<Utc>,
) -> (
  DateTime<Local>,
  DateTime<Local>,
  DateTime<Local>,
  DateTime<Local>,
) {
  fn parse_local_time(input: &str, now: &DateTime<Utc>) -> DateTime<Local> {
    let time = NaiveTime::parse_from_str(input, "%I:%M %p").unwrap_or_default();
    let date = now.date_naive();
    let dt = NaiveDateTime::new(date, time);

    Local::from_local_datetime(&Local::now().timezone(), &dt)
      .single()
      .unwrap()
  }

  debug!("1: {}", today.sunrise);
  let today_sunrise = parse_local_time(&today.sunrise, now);
  debug!("2: {}", today.sunset);
  let today_sunset = parse_local_time(&today.sunset, now);
  debug!("3: {}", today.moonrise);
  let today_moonrise = parse_local_time(&today.moonrise, now);
  debug!("4: {}", today.moonset);
  let today_moonset = parse_local_time(&today.moonset, now);

  let tm = *now + Duration::days(1);

  debug!("5: {}", tomorrow.sunrise);
  let tomorrow_sunrise = parse_local_time(&tomorrow.sunrise, &tm);
  debug!("6: {}", tomorrow.sunset);
  let tomorrow_sunset = parse_local_time(&tomorrow.sunset, &tm);
  debug!("7: {}", tomorrow.moonrise);
  let tomorrow_moonrise = parse_local_time(&tomorrow.moonrise, &tm);
  debug!("8: {}", tomorrow.moonset);
  let tomorrow_moonset = parse_local_time(&tomorrow.moonset, &tm);

  let next_sunrise = if *now < today_sunrise {
    today_sunrise
  } else {
    tomorrow_sunrise
  };

  let next_sunset = if *now < today_sunset {
    today_sunset
  } else {
    tomorrow_sunset
  };

  let next_moonrise = if *now < today_moonrise {
    today_moonrise
  } else {
    tomorrow_moonrise
  };

  let next_moonset = if *now < today_moonset {
    today_moonset
  } else {
    tomorrow_moonset
  };

  (next_sunrise, next_sunset, next_moonrise, next_moonset)
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub(crate) enum CloudyWeatherDataPoint {
  Int(i32),
  Dec(Decimal),
  Str(String),
  Time(DateTime<Local>),
}

impl std::fmt::Display for CloudyWeatherDataPoint {
  fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
    use CloudyWeatherDataPoint::*;
    let result = match self {
      Int(x) => x.to_string(),
      Dec(x) => x.to_string(),
      Str(x) => x.to_string(),
      Time(x) => x.format("%H:%M").to_string(),
    };

    write!(f, "{}", result)
  }
}

#[serde_as]
#[derive(Debug, Clone, Deserialize, Serialize)]
pub(crate) struct CurrentCondition {
  #[serde(rename = "FeelsLikeF")]
  #[serde_as(as = "DisplayFromStr")]
  pub feels_like_f: i32,

  #[serde(rename = "FeelsLikeC")]
  #[serde_as(as = "DisplayFromStr")]
  feels_like_c: i32,

  #[serde_as(as = "DisplayFromStr")]
  cloudcover: i32,

  #[serde_as(as = "DisplayFromStr")]
  pub humidity: i32,

  #[serde(rename = "localObsDateTime")]
  local_obs_datetime: String,

  #[serde(rename = "precipInches")]
  #[serde_as(as = "DisplayFromStr")]
  pub precip_inches: Decimal,

  #[serde(rename = "precipMM")]
  #[serde_as(as = "DisplayFromStr")]
  precip_mm: Decimal,

  #[serde_as(as = "DisplayFromStr")]
  pressure: String,

  #[serde(rename = "pressureInches")]
  #[serde_as(as = "DisplayFromStr")]
  pressure_inches: String,

  #[serde(rename = "temp_F")]
  #[serde_as(as = "DisplayFromStr")]
  pub temp_f: i32,

  #[serde(rename = "temp_C")]
  #[serde_as(as = "DisplayFromStr")]
  temp_c: i32,

  #[serde(rename = "uvIndex")]
  #[serde_as(as = "DisplayFromStr")]
  uv_index: i32,

  #[serde_as(as = "DisplayFromStr")]
  visibility: i32,

  #[serde(rename = "visibilityMiles")]
  #[serde_as(as = "DisplayFromStr")]
  pub visibility_miles: i32,

  #[serde(rename = "weatherCode")]
  #[serde_as(as = "DisplayFromStr")]
  pub weather_code: i32,

  #[serde(rename = "weatherDesc")]
  pub weather_desc: Vec<WeatherDescription>,

  #[serde(rename = "winddir16Point")]
  wind_dir_16_point: String,

  #[serde(rename = "winddirDegree")]
  #[serde_as(as = "DisplayFromStr")]
  wind_dir_degree: i32,

  #[serde(rename = "windspeedKmph")]
  #[serde_as(as = "DisplayFromStr")]
  wind_speed_kmph: i32,

  #[serde(rename = "windspeedMiles")]
  #[serde_as(as = "DisplayFromStr")]
  pub wind_speed_miles: i32,
}

#[serde_as]
#[derive(Debug, Deserialize, Serialize, Clone)]
pub(crate) struct Area {
  #[serde(rename = "areaName")]
  pub area_name: Vec<Location>,

  country: Vec<Location>,

  #[serde_as(as = "DisplayFromStr")]
  latitude: Decimal,

  #[serde_as(as = "DisplayFromStr")]
  longitude: Decimal,

  #[serde_as(as = "DisplayFromStr")]
  population: i32,

  pub region: Vec<Location>,
}

#[serde_as]
#[derive(Debug, Clone, Deserialize, Serialize)]
struct LocationRequest {
  query: String,

  #[serde(rename = "type")]
  location_type: String,
}

#[derive(Debug, Clone, Deserialize, Serialize)]
pub(crate) struct Location {
  pub value: String,
}

#[derive(Debug, Clone, Deserialize, Serialize)]
pub(crate) struct WeatherDescription {
  pub value: String,
}

#[serde_as]
#[derive(Clone, Debug, Deserialize, Serialize)]
struct Daily {
  astronomy: Vec<WttrAstronomy>,

  #[serde(rename = "avgtempC")]
  #[serde_as(as = "DisplayFromStr")]
  avg_temp_c: i32,

  #[serde(rename = "avgtempF")]
  #[serde_as(as = "DisplayFromStr")]
  avg_temp_f: i32,

  date: String,

  hourly: Vec<Hourly>,

  #[serde(rename = "maxtempC")]
  #[serde_as(as = "DisplayFromStr")]
  max_temp_c: i32,

  #[serde(rename = "maxtempF")]
  #[serde_as(as = "DisplayFromStr")]
  max_temp_f: i32,

  #[serde(rename = "mintempC")]
  #[serde_as(as = "DisplayFromStr")]
  min_temp_c: i32,

  #[serde(rename = "mintempF")]
  #[serde_as(as = "DisplayFromStr")]
  min_temp_f: i32,

  #[serde(rename = "sunHour")]
  #[serde_as(as = "DisplayFromStr")]
  sun_hour: Decimal,

  #[serde(rename = "totalSnow_cm")]
  #[serde_as(as = "DisplayFromStr")]
  total_snow_cm: Decimal,

  #[serde(rename = "uvIndex")]
  #[serde_as(as = "DisplayFromStr")]
  uv_index: i32,
}

#[serde_as]
#[derive(Clone, Debug, Deserialize, Serialize)]
struct WttrAstronomy {
  #[serde_as(as = "DisplayFromStr")]
  moon_illumination: i32,
  moon_phase: String,
  moonrise: String,
  moonset: String,
  sunrise: String,
  sunset: String,
}

#[serde_as]
#[derive(Clone, Debug, Deserialize, Serialize)]
struct Hourly {
  #[serde(rename = "DewPointC")]
  #[serde_as(as = "DisplayFromStr")]
  dew_point_c: i32,

  #[serde(rename = "DewPointF")]
  #[serde_as(as = "DisplayFromStr")]
  dew_point_f: i32,

  #[serde(rename = "FeelsLikeF")]
  #[serde_as(as = "DisplayFromStr")]
  feels_like_f: i32,

  #[serde(rename = "FeelsLikeC")]
  #[serde_as(as = "DisplayFromStr")]
  feels_like_c: i32,

  #[serde(rename = "HeatIndexC")]
  #[serde_as(as = "DisplayFromStr")]
  heat_index_c: i32,

  #[serde(rename = "HeatIndexF")]
  #[serde_as(as = "DisplayFromStr")]
  heat_index_f: i32,

  #[serde(rename = "WindChillC")]
  #[serde_as(as = "DisplayFromStr")]
  wind_chill_c: i32,

  #[serde(rename = "WindChillF")]
  #[serde_as(as = "DisplayFromStr")]
  wind_chill_f: i32,

  #[serde(rename = "WindGustKmph")]
  #[serde_as(as = "DisplayFromStr")]
  wind_gust_kmph: i32,

  #[serde(rename = "WindGustMiles")]
  #[serde_as(as = "DisplayFromStr")]
  wind_gust_miles: i32,

  #[serde_as(as = "DisplayFromStr")]
  chanceoffog: i32,

  #[serde_as(as = "DisplayFromStr")]
  chanceoffrost: i32,

  #[serde_as(as = "DisplayFromStr")]
  chanceofhightemp: i32,

  #[serde_as(as = "DisplayFromStr")]
  chanceofovercast: i32,

  #[serde_as(as = "DisplayFromStr")]
  chanceofrain: i32,

  #[serde_as(as = "DisplayFromStr")]
  chanceofremdry: i32,

  #[serde_as(as = "DisplayFromStr")]
  chanceofsnow: i32,

  #[serde_as(as = "DisplayFromStr")]
  chanceofsunshine: i32,

  #[serde_as(as = "DisplayFromStr")]
  chanceofthunder: i32,

  #[serde_as(as = "DisplayFromStr")]
  chanceofwindy: i32,

  #[serde_as(as = "DisplayFromStr")]
  cloudcover: i32,

  #[serde_as(as = "DisplayFromStr")]
  humidity: i32,

  #[serde(rename = "precipInches")]
  #[serde_as(as = "DisplayFromStr")]
  precip_inches: Decimal,

  #[serde(rename = "precipMM")]
  #[serde_as(as = "DisplayFromStr")]
  precip_mm: Decimal,

  #[serde_as(as = "DisplayFromStr")]
  pressure: String,

  #[serde(rename = "pressureInches")]
  #[serde_as(as = "DisplayFromStr")]
  pressure_inches: String,

  #[serde(rename = "tempF")]
  #[serde_as(as = "DisplayFromStr")]
  temp_f: i32,

  #[serde(rename = "tempC")]
  #[serde_as(as = "DisplayFromStr")]
  temp_c: i32,

  #[serde_as(as = "DisplayFromStr")]
  time: i32,

  #[serde(rename = "uvIndex")]
  #[serde_as(as = "DisplayFromStr")]
  uv_index: i32,

  #[serde_as(as = "DisplayFromStr")]
  visibility: i32,

  #[serde(rename = "visibilityMiles")]
  #[serde_as(as = "DisplayFromStr")]
  visibility_miles: i32,

  #[serde(rename = "weatherCode")]
  #[serde_as(as = "DisplayFromStr")]
  pub weather_code: i32,

  #[serde(rename = "weatherDesc")]
  weather_desc: Vec<WeatherDescription>,

  #[serde(rename = "winddir16Point")]
  wind_dir_16_point: String,

  #[serde(rename = "winddirDegree")]
  #[serde_as(as = "DisplayFromStr")]
  wind_dir_degree: i32,

  #[serde(rename = "windspeedKmph")]
  #[serde_as(as = "DisplayFromStr")]
  wind_speed_kmph: i32,

  #[serde(rename = "windspeedMiles")]
  #[serde_as(as = "DisplayFromStr")]
  wind_speed_miles: i32,
}
