use crate::CACHE_DIR_FOLDER_NAME;
use dirs::cache_dir;
use log::{debug, info};
use std::{
    fs::{read_dir, remove_file},
    path::PathBuf,
};

pub(crate) fn get_weather_icon(weather_code: i32) -> String {
    match weather_code {
        113 => "",                                                 // sunny
        116 => "",                                                 // partly cloudy
        119 => "",                                                 // cloudy
        122 => "",                                                 // overcast
        143 | 248 | 260 => "",                                     // fog
        176 | 263 | 266 | 293 | 296 | 299 | 302 | 305 | 308 => "", // rain
        179 | 227 | 323 | 326 => "",                               // snow
        182 => "",                                                 // sleet
        185 | 281 | 284 | 311 => "",                               // mix
        200 => "",                                                 // thunder
        _ => "???",
    }
    .to_string()
}

pub(crate) fn purge_cache() {
    info!("Deleting everything in the cache folder...");

    let mut path = PathBuf::new();

    let cache_dir = cache_dir().unwrap();
    path.push(&cache_dir);

    path.push(CACHE_DIR_FOLDER_NAME);

    if let Ok(directory) = read_dir(path) {
        for file in directory.filter_map(|f| f.ok()) {
            if let Ok(()) = remove_file(&file.path()) {
                debug!("Removed {:?}", file.path());
            } else {
                debug!("Could not remove {:?}", file.path());
            }
        }
    }
}
