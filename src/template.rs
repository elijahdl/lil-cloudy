use std::collections::HashMap;

use log::debug;

use crate::{CloudyError, WeatherReport};

pub(crate) fn process_template_vec(
    template: &[String],
    weather: &HashMap<String, WeatherReport>,
) -> String {
    template
        .iter()
        .map(|l| process_tempate_line(l).render_template(weather.clone()))
        .fold(String::new(), |acc, s| format!("{}{}\n", acc, s))
        .trim_end_matches('\n')
        .to_string()
}

fn process_tempate_line(input: &str) -> ParsedTemplate {
    let mut tokens = Vec::new();
    let mut buffer = String::new();
    let mut in_placeholder = false;
    let mut placeholder_location = String::new();
    let mut placeholder_value;
    let mut first_token = true;

    for c in input.chars() {
        if in_placeholder {
            if c == ':' {
                placeholder_location = buffer.clone();
                buffer.clear();
            } else if c == '}' {
                placeholder_value = buffer.clone();
                tokens.push(CloudyToken::Placeholder(
                    placeholder_location.clone(),
                    placeholder_value.clone(),
                ));
                buffer.clear();
                in_placeholder = false;
            } else {
                buffer.push(c);
            }
        } else if c == '{' {
            if !first_token || !buffer.is_empty() {
                tokens.push(CloudyToken::Text(buffer.clone()));
            }
            buffer.clear();
            in_placeholder = true;
        } else {
            buffer.push(c);
        }
        first_token = false;
    }

    if !buffer.is_empty() {
        tokens.push(CloudyToken::Text(buffer));
    }

    debug!("Parsed Template: {:?}", tokens);

    ParsedTemplate { tokens }
}

#[derive(Debug)]
struct ParsedTemplate {
    tokens: Vec<CloudyToken>,
}

impl ParsedTemplate {
    fn render_template(&self, weather: HashMap<String, WeatherReport>) -> String {
        self.tokens
            .iter()
            .map(|t| t.render_token(weather.clone()))
            .collect()
    }
}

#[derive(Debug)]
enum CloudyToken {
    Text(String),
    Placeholder(String, String), // location identifier, weather componenet
}

impl CloudyToken {
    fn render_token(&self, weather: HashMap<String, WeatherReport>) -> String {
        match self {
            CloudyToken::Text(text) => text.clone(),
            CloudyToken::Placeholder(location, variable) => {
                process_placeholder(location, variable, &weather).unwrap()
            }
        }
    }
}

fn process_placeholder(
    location: &str,
    variable: &str,
    weather: &HashMap<String, WeatherReport>,
) -> Result<String, CloudyError> {
    weather
        .get(location)
        .ok_or(CloudyError::LocationNotFoundError(location.to_string()))?
        .get(variable)
        .ok_or(CloudyError::VariableNotFoundError(variable.to_string()))
        .map(|p| p.to_string())
}
