use chrono::prelude::*;
use dirs::cache_dir;
use log::debug;
use reqwest::blocking::Client;

use std::{
    fs::{self, create_dir_all, File},
    io::Write,
    path::{Path, PathBuf},
};

use crate::{
    configuration::CloudyConfiguration,
    error::CloudyError,
    model::{CloudyWeatherData, WttrCache, WttrModel},
    WeatherReport, CACHE_DIR_FOLDER_NAME, CACHE_FILE_EXT, CACHE_FILE_NAME,
};

fn load_weather_from_cache(
    cache_file_path: &Path,
    now: &DateTime<Utc>,
    cache_timeout_minutes: u64,
) -> Result<WttrCache, CloudyError> {
    debug!("Attempting to read cache file at {:?}", cache_file_path);
    let cache_contents =
        fs::read_to_string(cache_file_path).map_err(|_| CloudyError::CacheFileLoadError)?;

    debug!("Attempting to deserialize cached weather data");
    let weather_data: WttrCache = serde_json::from_str(&cache_contents)
        .map_err(|_| CloudyError::CacheDeserializationError)?;

    if (*now - weather_data.last_fetch_time).num_minutes() as u64 >= cache_timeout_minutes {
        log::debug!("Cache is expired!");
        Err(CloudyError::CacheExpiredMessage)
    } else {
        log::debug!("Successfully loaded cached weather data");
        Ok(weather_data)
    }
}

pub(crate) fn get_weather_for_location(
    refresh_flag: bool,
    config: &CloudyConfiguration,
    location: (&str, &str), // identifier, actual search term
    client: &Client,        //
) -> Result<WeatherReport, CloudyError> {
    let cache_file_path = make_cache_file_path(location.1)?;
    let now = Utc::now();

    if !refresh_flag {
        if let Ok(app_data) =
            load_weather_from_cache(&cache_file_path, &now, config.general.cache_timeout_minutes)
        {
            return Ok(app_data.weather);
        }
    }

    let url = format!(
        "https://{}/{}?2&format=j1",
        config.general.wttr_endpoint, location.1
    );
    debug!("Request URL: {}", url);

    let resp = client
        .get(url)
        .send()
        .map_err(|_| CloudyError::WeatherDataDownloadError)?;

    let weather = serde_json::from_str::<WttrModel>(
        &resp.text().map_err(|_| CloudyError::WeatherDataReadError)?,
    )
    .map_err(|_| CloudyError::CacheSeriailizationError)?
    .create_cloudy_weather_data(&now);

    let cache = WttrCache {
        weather,
        last_fetch_time: now,
    };

    debug!("Writing cache file {:?}", cache_file_path);
    let mut output = File::create(&cache_file_path)
        .map_err(|_| CloudyError::CacheFileCreateError(cache_file_path.display().to_string()))?;
    write!(
        output,
        "{}",
        serde_json::to_string(&cache).map_err(|_| CloudyError::CacheSeriailizationError)?
    )
    .map_err(|_| CloudyError::CacheFileWriteError)?;

    Ok(cache.weather)
}

fn make_cache_file_path(location: &str) -> Result<PathBuf, CloudyError> {
    let mut path = PathBuf::new();

    let cache_dir = cache_dir().unwrap();
    path.push(&cache_dir);

    path.push(CACHE_DIR_FOLDER_NAME);
    create_dir_all(&path)
        .map_err(|_| CloudyError::CacheDirCreateError(cache_dir.display().to_string()))?;

    let filename = format!("{}-{}.{}", CACHE_FILE_NAME, location, CACHE_FILE_EXT);
    path.push(filename);

    Ok(path)
}
