use std::error::Error;

#[derive(Debug)]
pub enum CloudyError {
    CacheDeserializationError,
    CacheDirCreateError(String),
    CacheExpiredMessage,
    CacheFileCreateError(String),
    CacheFileLoadError,
    CacheFileWriteError,
    CacheSeriailizationError,
    ConfigDeserializationError,
    ConfigDirCreateError(String),
    ConfigFileCreateError(String),
    ConfigFileLoadError,
    ConfigSeriailizationError,
    LocationNotFoundError(String),
    SlideIndexOutOfBounds(u64, u64),
    VariableNotFoundError(String),
    WeatherDataDownloadError,
    WeatherDataReadError,
}

impl Error for CloudyError {}

impl std::fmt::Display for CloudyError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        use CloudyError::*;
        match self {
            CacheDeserializationError => write!(f, "Cache Deserialization Error"),
            CacheDirCreateError(m) => write!(f, "Cache Dir Create Error: {}", m),
            CacheExpiredMessage => write!(f, "Cache Expired"),
            CacheFileCreateError(m) => write!(f, "Cache File Create Error: {}", m),
            CacheFileLoadError => write!(f, "Cache File Load Error"),
            CacheFileWriteError => write!(f, "Cache File Write Error"),
            CacheSeriailizationError => write!(f, "Cache Serialization Error"),
            ConfigDeserializationError => write!(f, "Config Deserialization Error"),
            ConfigDirCreateError(m) => write!(f, "Config File Create Error: {}", m),
            ConfigFileCreateError(m) => write!(f, "Config File Create Error: {}", m),
            ConfigFileLoadError => write!(f, "Cache File Load Error"),
            ConfigSeriailizationError => write!(f, "Cache Serialization Error"),
            LocationNotFoundError(m) => write!(f, "Could not find location {} in weather data", m),
            SlideIndexOutOfBounds(slides_length, user_index) => write!(
                f,
                "Selected slide {} does not exist ({} slides available)",
                user_index, slides_length
            ),
            VariableNotFoundError(m) => write!(f, "Could not find variable {} in weather data", m),
            WeatherDataDownloadError => write!(f, "Weather Data Download Error"),
            WeatherDataReadError => write!(f, "Weather Data Read Error"),
        }
    }
}
