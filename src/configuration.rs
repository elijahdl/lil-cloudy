use dirs::config_dir;
use log::{debug, warn};
use maplit::hashmap;
use serde::{Deserialize, Serialize};
use std::{
    collections::HashMap,
    fs::{create_dir_all, read_to_string, File},
    io::Write,
    path::{Path, PathBuf},
};

use crate::{Args, CloudyError};

const CONFIG_FILE_NAME: &str = "lil-cloudy-config.toml";
const CONFIG_DIR_FOLDER_NAME: &str = "lil-cloudy";

#[derive(Clone, Debug, Deserialize, Serialize)]
pub(crate) struct CloudyConfiguration {
    pub general: GeneralOptions,
    pub locations: HashMap<String, String>, // location identifier, location name (leave blank for current location)
    pub slides: Vec<String>,
    pub tooltip: Vec<String>, // Locations separated by newline
}

impl CloudyConfiguration {
    pub(crate) fn override_config_with_args(self, args: &Args) -> Self {
        let slide_id = args.slides_id.to_owned().unwrap_or(self.general.slide_id);

        let general = GeneralOptions {
            slide_id,
            ..self.general
        };

        CloudyConfiguration { general, ..self }
    }

    pub(crate) fn add_local_location(self) -> Self {
        let mut locations = self.locations.clone();
        if !locations.contains_key("") {
            locations.insert("".to_string(), "".to_string());
        }
        Self { locations, ..self }
    }
}

impl Default for CloudyConfiguration {
    fn default() -> Self {
        let locations = hashmap! {};

        let slides = vec![
            "{current_weather_icon} {current_temp_f}".to_string(),
            "{current_weather_description}".to_string(),
        ];

        let tooltip = vec![
            "{area_name}: {current_temp_f}".to_string(),
            "{current_weather_description}".to_string(),
        ];

        Self {
            general: Default::default(),
            locations,
            slides,
            tooltip,
        }
    }
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub(crate) struct GeneralOptions {
    pub cache_timeout_minutes: u64,
    pub program_runs_per_slide: u64,
    pub wttr_endpoint: String,
    pub slide_id: String,
}

impl Default for GeneralOptions {
    fn default() -> Self {
        Self {
            cache_timeout_minutes: 15,
            program_runs_per_slide: 1,
            wttr_endpoint: "wttr.in".to_string(),
            slide_id: "default".to_string(),
        }
    }
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub(crate) struct LocationTemplatePairing {
    pub location: String,
    pub template: String,
}

fn load_config_from_file(config_file_path: &Path) -> Result<CloudyConfiguration, CloudyError> {
    debug!(
        "Attempting to read configuration file at {:?}",
        config_file_path
    );
    let config_contents =
        read_to_string(config_file_path).map_err(|_| CloudyError::ConfigFileLoadError)?;

    debug!("Attempting to deserialize loaded configuration file");
    let config: CloudyConfiguration = toml::from_str(&config_contents)
        .map_err(|_| CloudyError::ConfigDeserializationError)
        .unwrap();

    Ok(config)
}

pub(crate) fn get_config(config_path: Option<PathBuf>) -> Result<CloudyConfiguration, CloudyError> {
    let config_file_path = if let Some(path) = config_path {
        path
    } else {
        make_config_file_path()?
    };

    if let Ok(config) = load_config_from_file(&config_file_path) {
        debug!("Successfully read configuration file");
        return Ok(config);
    }

    warn!(
        "No config file found! Creating a new configuration file at {}",
        config_file_path.display()
    );

    let config = CloudyConfiguration::default();

    debug!("Writing new configuration file to {:?}", config_file_path);
    let mut output = File::create(&config_file_path)
        .map_err(|_| CloudyError::ConfigFileCreateError(config_file_path.display().to_string()))?;
    write!(
        output,
        "{}",
        toml::to_string(&config).map_err(|_| CloudyError::ConfigSeriailizationError)?
    )
    .map_err(|_| CloudyError::CacheFileWriteError)?;

    Ok(config)
}

fn make_config_file_path() -> Result<PathBuf, CloudyError> {
    let mut path = PathBuf::new();

    let config_dir = config_dir().unwrap();
    path.push(&config_dir);

    path.push(CONFIG_DIR_FOLDER_NAME);
    create_dir_all(&path)
        .map_err(|_| CloudyError::ConfigDirCreateError(config_dir.clone().display().to_string()))?;

    path.push(CONFIG_FILE_NAME);

    Ok(path)
}
