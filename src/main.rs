use std::{collections::HashMap, path::PathBuf, time::Duration};

use clap::Parser;
use configuration::get_config;
use log::debug;

mod configuration;

mod model;
use model::CloudyWeatherDataPoint;

mod error;
use error::CloudyError;

mod render;
use render::render_weather_output;

mod template;
mod util;

mod weather;
use reqwest::blocking::Client;
use weather::get_weather_for_location;

#[cfg(not(target_env = "msvc"))]
use tikv_jemallocator::Jemalloc;

#[cfg(not(target_env = "msvc"))]
#[global_allocator]
static GLOBAL: Jemalloc = Jemalloc;

const CACHE_FILE_NAME: &str = "lil-cloudy-cache";
const CACHE_FILE_EXT: &str = "json";
const CACHE_DIR_FOLDER_NAME: &str = "lil-cloudy";

type WeatherReport = HashMap<String, CloudyWeatherDataPoint>;

#[derive(Parser, Debug, Clone)]
#[clap(author = "Elijah Love", version, about)]
/// Application configuration
struct Args {
    /// Print debug information
    #[arg(short = 'd')]
    debug: bool,

    /// Ignore the cache, force download weather data
    #[arg(short = 'f')]
    force_refresh: bool,

    /// Output a weather report in JSON format for waybar
    #[arg(short = 'j')]
    json_format: bool,

    /// Specify the configuration file to use
    #[arg(short = 'c')]
    config_file: Option<String>,

    /// Purge all files from local cache
    #[arg(long = "purge-cache", conflicts_with_all=&["config_file", "json_format", "force_refresh"])]
    purge_cache: bool,

    /// Id of the slide tracker to use
    #[arg(long = "slide-id", short = 'i')]
    slides_id: Option<String>,

    /// Number slide to use (1-indexed)
    #[arg(short = 's')]
    slide: Option<u64>,
}

fn main() {
    let args = Args::parse();

    let log_level = if args.debug {
        log::Level::Debug
    } else {
        log::Level::Info
    };

    simple_logger::init_with_level(log_level).unwrap();
    debug!("Debug logging on");
    debug!("Arguments: {:?}", args);

    if args.purge_cache {
        util::purge_cache();
        return;
    }

    let user_config = args.config_file.as_ref().map(PathBuf::from);

    let config = get_config(user_config)
        .unwrap()
        .override_config_with_args(&args)
        .add_local_location();

    debug!("Configuration: {:?}", config);

    let client = Client::builder()
        .timeout(Duration::from_secs(5))
        .build()
        .unwrap();

    let weather_reports: HashMap<_, _> = config
        .locations
        .iter()
        .filter_map(|(i, l)| {
            debug!("i: {:?}, l: {:?}", i, l);
            get_weather_for_location(args.force_refresh, &config, (&i, &l), &client)
                .ok()
                .map(|m| (i.clone(), m))
        })
        .collect();

    println!(
        "{}",
        render_weather_output(&weather_reports, &args, &config).unwrap()
    );
}
