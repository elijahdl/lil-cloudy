use crate::{
    configuration::CloudyConfiguration, template::process_template_vec, Args, CloudyError,
    WeatherReport, CACHE_DIR_FOLDER_NAME,
};

use dirs::cache_dir;
use log::debug;
use serde::{Deserialize, Serialize};
use std::{collections::HashMap, fs::create_dir_all, fs::File, io::Write, path::PathBuf};

#[derive(Debug, Serialize, Deserialize)]
pub(crate) struct WaybarOutput {
    pub text: String,
    pub tooltip: String,
}

impl WaybarOutput {
    pub fn serialize_to_json(&self) -> String {
        serde_json::to_string(self).unwrap()
    }
}

pub(crate) fn render_weather_output(
    weather: &HashMap<String, WeatherReport>,
    args: &Args,
    config: &CloudyConfiguration,
) -> Result<String, CloudyError> {
    let text = process_template_vec(&config.slides, weather);
    let text = pick_slide_from_lines(
        &text,
        config.general.program_runs_per_slide,
        &config.general.slide_id,
        args.slide,
    )?
    .to_string();

    if args.json_format {
        debug!("Outputting to waybar json format");
        let tooltip = process_template_vec(&config.tooltip, weather);
        Ok(WaybarOutput { text, tooltip }.serialize_to_json())
    } else {
        Ok(text)
    }
}

fn pick_slide_from_lines<'a>(
    input: &'a str,
    program_runs_per_slide: u64,
    slide_id: &'a str,
    user_slide: Option<u64>,
) -> Result<&'a str, CloudyError> {
    let lines = input.lines().collect::<Vec<_>>();
    let length = lines.len() as u64;
    if let Some(index) = user_slide {
        if index > length || index <= 0 {
            Err(CloudyError::SlideIndexOutOfBounds(length, index))
        } else {
            Ok(lines[index as usize - 1])
        }
    } else {
        let length = length * program_runs_per_slide;
        let slide_file_path = make_slide_tracker_file_path(slide_id).unwrap();

        let current_slide = read_slide_number_from_file(&slide_file_path).unwrap_or(0);

        let next_slide = if current_slide >= length {
            1
        } else {
            current_slide + 1
        } as f64;

        let mut output = File::create(&slide_file_path)
            .map_err(|_| CloudyError::CacheFileCreateError(slide_file_path.display().to_string()))
            .unwrap();
        write!(output, "{}", next_slide)
            .map_err(|_| CloudyError::CacheFileWriteError)
            .unwrap();

        let index = ((next_slide / program_runs_per_slide as f64).ceil() - 1.0) as usize;

        Ok(lines[index])
    }
}

fn read_slide_number_from_file(path: &PathBuf) -> Result<u64, CloudyError> {
    std::fs::read_to_string(path)
        .map_err(|_| CloudyError::CacheFileLoadError)?
        .parse()
        .map_err(|_| CloudyError::CacheDeserializationError)
}

fn make_slide_tracker_file_path(slide_id: &str) -> Result<PathBuf, CloudyError> {
    let mut path = PathBuf::new();

    let cache_dir = cache_dir().unwrap();
    path.push(&cache_dir);

    path.push(CACHE_DIR_FOLDER_NAME);
    create_dir_all(&path)
        .map_err(|_| CloudyError::CacheDirCreateError(cache_dir.display().to_string()))?;

    let filename = format!("slide-{}", slide_id);
    path.push(filename);

    Ok(path)
}
